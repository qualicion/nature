package com.nature.core.pageobjects;

import com.nature.core.DriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.nature.core.dsl.WebDriverDsl.waitForNewPage;

public class NewsAndViewsPage {

    private static WebDriver driver = DriverManager.getInstance().getDriver();

    @FindBy(xpath = "/html[1]/body[1]/div[4]/section[1]/article[1]/div[1]/div[1]/header[1]/div[1]")
    private WebElement news_and_views;


    public NewsAndViewsPage waitForNewsAndViewsPage() {
        waitForNewPage(news_and_views);
        return this;
    }

    public NewsAndViewsPage and() {
        return this;
    }
}
