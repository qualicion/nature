package com.nature.functionaltest;


import com.nature.core.pageobjects.HomePage;
import com.nature.core.pageobjects.NewsAndViewsPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class NatureHomePageTest{

    private HomePage homePage = new HomePage();
    private NewsAndViewsPage newsAndViews = new NewsAndViewsPage();


        @Given("^I am on the Nature homepage$")
         public void NatureHomePageTest() {
            homePage.open();
    }


        @When("^I click on the link to the content on the banner$")
        public void iClickOnTheLinkToTheContentOnTheBanner() {
            homePage.waitForPage().and().click_banner_link();
    }


        @Then("^I should be able to view the page$")
        public void iShouldBeAbleToViewThePage(){
            newsAndViews.waitForNewsAndViewsPage();
    }
}
