@home
  Feature: Home page
    Scenario: Navigate to homepage and browse main content
      Given I am on the Nature homepage
      When I click on the link to the content on the banner
      Then I should be able to view the page



      Scenario: Navigate to other contents
        Given I am on the main banner page
        When I click on the nature homepage link
        And I click on the other contents
        Then I should be able to view other content page



